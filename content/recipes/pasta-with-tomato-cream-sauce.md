---
title: "Pasta with Tomato Cream Sauce"
date: "2021-10-11"
description: "Quick and Easy Pasta with Tomato Cream Sauce"
tags: ["vegetarian", "onions", "pasta", "tomato", "cream", "stock"]
---
## Ingredients

For 2 people:

* 250 g spaghetti
* 200ml cream
* 2-3 medium sized tomatoes
* some chopped red onions
* a bit of vegetable stock
* basil
* 1 table spoon of olive oil
* 1 table spoon of soft cheese

## Method of preparation

Put the Spagetti in boiling water. Chop the tomatoes and the two to
three slices of red onions. Fry everything shortly in the olive oil.
Then add the cream and the soft cheese. Add some vegetable stock and the
herbs, preferably basil. Add salt and pepper to taste.

Enjoy your easy-to-make pasta!

