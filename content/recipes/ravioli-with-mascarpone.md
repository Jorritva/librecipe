---
title: "Ravioli with Mascarpone"
date: "2022-06-27"
description: ""
tags: ["Ravioli", "Italian", "Mascarpone"]
---
## Ingredients

For two persons:

* 200 grams of minced meat
* Ravioli
* 1 Bell pepper
* Mascarpone
* 1 Leek
* Optional: Grated cheese

## Method of preparation

Cut the bell pepper, the leek, and bake the minced meat. Add the ravioli, mascarpone, bell pepper and leek to the meat.

Optionally serve with grated cheese. Enjoy!
