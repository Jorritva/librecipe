---
title: "Rice with beans"
date: "2021-10-02"
description: "Vegetarian rice with beans, peppers and tomatoes. Served with salad."
tags: ["vegetarian", "rice"]
---
## Ingredients

For two persons:

* 1 clove of garlic
* 2 onion
* 150 grams brown rice
* 2 sweet peppers
* chili pepper
* 1 cucumber
* can of peeled tomatoes
* can of kidney beans

## Method of preparation

Cut the garlic and finely chop the onions. Saute the onion and garlic together with part of the chili pepper. Add the rice and 300 ml of water. Bring this to a boil and cook for 5 minutes. Cube the bell peppers, add them to the rice and cook for 5-10 minutes until the rice is done. 

In the meantime, prepare a salad using vinegar, the remaining chili pepper and the sliced cucumber. Rinse and drain the kidney beans and add them together with the tomatoes to the rice. Season with either thyme or oregano. Once properly heated, serve the rice and the salad.
