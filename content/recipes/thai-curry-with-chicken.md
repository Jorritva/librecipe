---
title: "Thai curry with chicken"
date: "2021-03-06"
description: ""
tags: ["rice", "chicken", "stir-fry"]
---
## Ingredients

For four persons:

* 1 tablespoon of oil
* 300 grams marinated Thai chicken pieces
* 700 grams wok vegetables
* 400 milliliters coconut milk
* 600 grams rice

## Method of preparation

Heat up the oil in a wok and stir-fry the chicken pieces for 2 minutes over a high heat. Add the wok vegetables and stir-fry for another 3 minutes. Add the coconut milk, bring to the boil and let it boil for 5 minutes.

Prepare the rice according to the instructions on the packaging. Serve the rice with the Thai curry.
