---
title: "Vegetarian Lasagna"
date: "2022-06-27"
description: "A vegetarian take on the good old lasagna."
tags: ["lasagna", "vegetarian", "Italian"]
---
## Ingredients

For four persons:

* 2 cloves of garlic, fine slices
* large (red-)onion or 2 smaller ones, chopped
* oregano, thyme
* 1 zucchini in thin slices
* 4 large tomatoes in thin slices
* 2 cans of peeled and diced tomato 
* 200 grams rasped cheese
* 125 grams mozzarella
* lasagna sheets (enough for two layers)
* vegatarian minced meat (recommended: Beyond Meat)

## Method of preparation

Fruit the onions in a larger pan using some oil. Add oregano and thyme. Once fruited, add the minced meat and make sure to properly stir to create smaller pieces. Once the minced meat is done, add the 2 cans of tomatoes and optionally some salt and pepper.

Put some oil in a pan and add the garlic and bake untill slightly yellow. Add the very thin slices of zucchini to the pan and let it sear untill the zucchini is nice and flexible. Optioinally add some black pepper.

Use some paper towels to soak up the mozzarella. 

Grease the casserole with oil. Add a single layer of the lasagna sheets on the bottom of the casserole. On top of this, add a layer using half of the sliced tomatoes. Cover this with half of the sauce containing the minced meat and tomatoes. Add a layer of zucchini. Add 100 grams of rasped cheese. Repeat all layers from the lasagna sheets untill the zucchini. Before adding the remaining rasped cheese, create a layer of torn mozzarella. Bake in the oven at 180°C for 25-30 minutes. 

